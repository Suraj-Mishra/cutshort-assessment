import { IButtonProp } from "./interfaces";
import "./Button.module.scss";

function Button(props: IButtonProp): JSX.Element {
  const { text, style, handleNext } = props;
  return (
    <button onClick={handleNext} className={style}>
      {text}
    </button>
  );
}

export default Button;
