export interface IButtonProp {
  text: string;
  style: string;
  handleNext: (val: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
}
