import CustomInput from "../custom_input/CustomInput";
import { ICustomFormProps, IinputMetaData } from "./interfaces";
import style from "./CustomForm.module.scss";

function CustomForm(props: ICustomFormProps) {
  const { formAttributes } = props;

  return (
    <>
      <form>
        {formAttributes.map((inputMetaData: IinputMetaData, index) => {
          return (
            <div className={style.field} key={index}>
              <label htmlFor={inputMetaData.name} className={style.label}>
                {inputMetaData.label}
              </label>
              <CustomInput
                placeholder={inputMetaData.placeholder}
                required={inputMetaData.required}
                name={inputMetaData.name}
              />
            </div>
          );
        })}
      </form>
    </>
  );
}

export default CustomForm;
