export interface IinputMetaData {
  label: string;
  placeholder: string;
  required: boolean;
  name: string;
}

export interface ICustomFormProps {
  formAttributes: IinputMetaData[];
}
