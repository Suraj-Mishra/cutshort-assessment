export interface ICustomInputProps {
  required: boolean;
  placeholder: string;
  name: string;
}
