import { ICustomInputProps } from "./interfaces";
import "./CustomInput.module.scss";

function CustomInput(props: ICustomInputProps) {
  const { required = false, placeholder, name } = props;
  return (
    <input
      required={required}
      placeholder={placeholder}
      type="text"
      name={name}
    />
  );
}

export default CustomInput;
