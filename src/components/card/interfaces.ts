export interface ICardProps {
  border: string;
  imgSrc: string;
  heading: string;
  content: string;
  setSelectedCard: (val: string) => void;
}
