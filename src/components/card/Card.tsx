import { useRef } from "react";
import style from "./Card.module.scss";
import { ICardProps } from "./interfaces";

function Card(props: ICardProps) {
  const { imgSrc, heading, content, border, setSelectedCard } = props;

  return (
    <div
      onClick={() => setSelectedCard(heading)}
      className={style.container}
      style={{ boxShadow: border }}
    >
      <img src={imgSrc} />
      <p className={style.heading}>{heading}</p>
      <p className={style.content}>{content}</p>
    </div>
  );
}

export default Card;
