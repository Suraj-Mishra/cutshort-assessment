import { useState } from "react";
import style from "./App.module.scss";
import Setup from "./views/setup/Setup";
import Usage from "./views/usage/Usage";
import Welcome from "./views/welcome/Welcome";
import edenLogo from "./assets/edenLogo.png";
import Stepper from "@mui/material/Stepper";
import Step from "@mui/material/Step";
import StepButton from "@mui/material/StepButton";
import Button from "./components/button/Button";

const steps = ["1", "2", "3"];

function App() {
  const [activeStep, setActiveStep] = useState(0);

  function totalSteps() {
    return steps.length;
  }

  const isLastStep = () => {
    return activeStep === totalSteps() - 1;
  };

  function handleNext() {
    setActiveStep((prev) => (isLastStep() ? prev : prev + 1));
  }

  function PageContent() {
    switch (activeStep) {
      case 0:
        return <Welcome />;
      case 1:
        return <Setup />;
      case 2:
        return <Usage />;
      default:
        return <Welcome />;
    }
  }

  function handleStep(step: number) {
    return () => {
      setActiveStep(step);
    };
  }

  return (
    <div className={style.container}>
      <div className={style.heading}>
        <img src={edenLogo} />
        <Stepper className={style.stepper} nonLinear activeStep={activeStep}>
          {steps.map((label, index) => (
            <Step key={label}>
              <StepButton color="inherit" onClick={handleStep(index)} />
            </Step>
          ))}
        </Stepper>
      </div>
      <div className={style.pageContent}>{PageContent()}</div>
      <Button
        style={style.primaryButton}
        text={activeStep > 1 ? "Create Workspace" : "Launch Eden"}
        handleNext={activeStep !== totalSteps() ? handleNext : () => {}}
      />
    </div>
  );
}

export default App;
