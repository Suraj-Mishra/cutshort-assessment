import { useState } from "react";
import group from "../../assets/group.png";
import single from "../../assets/single.png";
import Card from "../../components/card/Card";
import style from "./Usage.module.scss";

function Usage() {
  const [selectedCard, setSelectedCard] = useState("");

  const heading = {
    card1: "For Myself",
    card2: "With my team",
  };

  const content = {
    card1: "Write better. Think more clearly. Stay organized.",
    card2: "Wikis, docs, tasks & projects, all in one place.",
  };

  return (
    <div className={style.container}>
      <h1>How are you planning to use Eden?</h1>
      <h2>We'll streamline your setup experience accordingly.</h2>
      <div className={style.cardsContainer}>
        <Card
          border={
            heading.card1 === selectedCard
              ? "0 0 0 1px blue"
              : "0 0 0 1px #edf1f6"
          }
          imgSrc={single}
          heading={heading.card1}
          content={content.card1}
          setSelectedCard={setSelectedCard}
        />
        <Card
          border={
            heading.card2 === selectedCard
              ? "0 0 0 1px blue"
              : "0 0 0 1px #edf1f6"
          }
          imgSrc={group}
          heading={heading.card2}
          content={content.card2}
          setSelectedCard={setSelectedCard}
        />
      </div>
    </div>
  );
}

export default Usage;
