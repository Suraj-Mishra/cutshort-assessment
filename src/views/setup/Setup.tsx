import CustomForm from "../../components/custom_form/CustomForm";
import style from "./Setup.module.scss";

const formAttributes = [
  {
    label: "Workspace Name",
    placeholder: "Eden",
    required: true,
    name: "workspaceName",
  },
];

function Setup() {
  return (
    <div className={style.container}>
      <div className={style.titles}>
        <h1>Let's set up a home for all your work</h1>
        <h2>You can always create another workspace later.</h2>
      </div>
      <div>
        <CustomForm formAttributes={formAttributes} />
        <div className={style.optionalValue}>
          <label htmlFor="workspace-url">
            Workspace URL <span>(optional)</span>
          </label>
          <div className={style.url}>
            <input disabled defaultValue="www.eden.com/" />
            <input id="workspace-url" placeholder="Example" />
          </div>
        </div>
      </div>
    </div>
  );
}

export default Setup;
