import CustomForm from "../../components/custom_form/CustomForm";
import style from "./Welcome.module.scss";

const formAttributes = [
  {
    label: "Full Name",
    placeholder: "Steve Jobs",
    required: true,
    name: "fullName",
  },
  {
    label: "Display Name",
    placeholder: "Steve",
    required: true,
    name: "fullName",
  },
];

function Welcome() {
  return (
    <div className={style.container}>
      <div className={style.titles}>
        <h1>Welcome! First things first...</h1>
        <h2>You can always change them later.</h2>
      </div>
      <div className={style.form}>
        <CustomForm formAttributes={formAttributes} />
      </div>
    </div>
  );
}

export default Welcome;
